--Exibir todos os produtos que custam mais que o pre�o m�dio de venda
--dos produtos
select p.descricao, p.precovenda from produtos p
where p.precovenda > (select avg(pr.precovenda) from produtos pr);

--Exibir todos os produtos de fornecedores de SP que custam menos
--que a m�dia dos produtos de fornecedores de MG
select p.descricao, p.precovenda
from produtos p
join fornecedores f on (p.codfornecedor = f.codfornecedor)
where f.estado = 'SP'
and p.precovenda < (select avg(pr.precovenda)
					from produtos pr
					join fornecedores fo on (pr.codfornecedor = fo.codfornecedor)
					where fo.estado = 'MG');
					
--Exibir o nome do fornecedor e o pre�o do produto mais caro
select 
	f.nome,
	(select max(p.precovenda) from produtos p where p.codfornecedor = f.codfornecedor) as produto_mais_caro
from fornecedores f;

--Retorna o c�digo e o nome de todos os clientes que fizeram compras
select c.codcliente, c.nome
from clientes c 
where exists (select 1 from compras co where co.codcliente = c.codcliente);

--Retorna o c�digo e o nome de todos os clientes que n�o fizeram compras
select c.codcliente, c.nome
from clientes c 
where not exists (select 1 from compras co where co.codcliente = c.codcliente);

--Selecionar os clientes que est�o nos mesmos estados dos fornecedores
select c.nome, c.estado from clientes c
where c.estado in (select distinct f.estado from fornecedores f);

--Compra mais antiga e compra mais recente
select c.nome, c.datacadastro, co.datacompra, 'Compra mais recente' as tipo
from clientes c
join compras co on (c.codcliente = co.codcliente)
where co.datacompra = (select max(com.datacompra) from compras com)
union
select c.nome, c.datacadastro, co.datacompra, 'Compra mais antiga' as tipo
from clientes c
join compras co on (c.codcliente = co.codcliente)
where co.datacompra = (select min(com.datacompra) from compras com)
order by 3, 1;

--Clientes que compraram �gua e abobrinha
select c.nome
	from clientes c 
	join compras co on (c.codcliente = co.codcliente)
	join comprasitens ci on (co.codcompra = ci.codcompra)
	join produtos p on (ci.codproduto = p.codproduto)
where p.descricao ilike '%�gua%'
intersect 
select c.nome
	from clientes c 
	join compras co on (c.codcliente = co.codcliente)
	join comprasitens ci on (co.codcompra = ci.codcompra)
	join produtos p on (ci.codproduto = p.codproduto)
where p.descricao ilike '%abobrinha%';

--Clientes que compraram biscoito mas n�o compraram �gua
select c.nome
	from clientes c 
	join compras co on (c.codcliente = co.codcliente)
	join comprasitens ci on (co.codcompra = ci.codcompra)
	join produtos p on (ci.codproduto = p.codproduto)
where p.descricao ilike '%biscoito%'
except
select c.nome
	from clientes c 
	join compras co on (c.codcliente = co.codcliente)
	join comprasitens ci on (co.codcompra = ci.codcompra)
	join produtos p on (ci.codproduto = p.codproduto)
where p.descricao ilike '%�gua%';

