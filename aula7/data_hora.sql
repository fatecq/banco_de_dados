--Converte uma strig para o tipo date
select date '2022-05-17';

--Acrescentar uma quantidade de dias a uma data
select date '2022-05-17' + interval '23 days' as data_final;

--Acrescentar dias e horas a uma data
select timestamp '2022-05-17 21:45' + interval '23 days 14 hours' as data_final;

--Acrescentar uma quantidade de semanas a uma data
select date '2022-05-17' + interval '7 days' as data_final;

--Exibir compras feitas no m�s de abril de 2020
select codcompra, datacompra 
from compras 
where datacompra between date '2020-04-01' and date '2020-04-30' + interval '45 days'
order by 1;

--Acrescentar 45 minutos na data e hora atuais
select now() + interval '45 minutes' as data_final;

--Data atual menos 75 anos
select now() - interval '75 years' as data_final;

--Tempo passado entre duas datas
select age(now(), date '2004-02-01 13:00');

--Extraindo dados da data
select extract(day from now());
select extract(month from now());
select extract (century from now());
select extract (hour from now());
select extract (minute from now());
select extract (dow from now()); -- dow: 'day of week'
select extract (doy from now()); -- doy: 'day of year'

--Lista todas as compras feitas no m�s de abril de 2020
select codcompra, datacompra
from compras c 
where extract (month from datacompra) = 4
and extract (year from datacompra) = 2020;

--to_date
select to_date('22/05/2022', 'DD/MM/YYYY');

set lc_time to 'pt_BR.UTF-8';
select to_date('22 de maio de 2002', 'DD "de" TMmonth "de" 2002');

--data para string
select to_char(date '2022-09-12', 'DD/MM/YYYY');

select to_char(now(), 'DD/MM/YYYY HH:mm')


