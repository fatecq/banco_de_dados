--Fun��o para calcular a m�dia
create function media(a numeric, b numeric) returns numeric
as
$$
declare
    media numeric;
begin
    media := (a + b) / 2;
    return media;
end;
$$
language plpgsql;

select media(10, 3);

drop function media(numeric, numeric);

create function media(a numeric, b numeric, c numeric) 
returns numeric as
$$
declare
    media numeric;
begin
    media := (a + b + c) / 3;
    return media;
end;
$$
language plpgsql;

select media(3,9);
select media(7,6,5);

-- Pseudo-tipos
-- %ROWTYPE
create function dados_cliente(cod int) returns int as
$$
declare
    dados clientes%rowtype;
begin
    select * into dados from clientes where codcliente = cod;
    
    raise notice '% - %', dados.nome, dados.estado;    
end;
$$
language plpgsql;
select dados_cliente(10);


-- RECORD
create or replace function compras_estado(est char(2)) 
returns int as
$$
declare
    compras record;
begin
    select c.estado, count(co.codcompra) as num_compras 
            into compras
    from clientes c join compras co on c.codcliente = co.codcliente
    where c.estado = est
    group by c.estado;
    
    raise notice '% - %', compras.estado, compras.num_compras;

    return 1;
end;
$$
language plpgsql;

select compras_estado('SP');

-- Dia 07/06/2021
create function num_compras1(cod int) returns int as
$$
declare
    num_compras int;
begin
    select count(codcompra) into num_compras
    from compras
    where codcliente = cod;
    
    raise notice 'O cliente % fez % compras', cod, num_compras;
    raise notice 'Devolvendo o valor calculado';
    
    return num_compras;
end;
$$
language plpgsql;

select num_compras1(2);

create function num_compras2(cod int) returns int as
$$
declare
    num_compras int;
begin
    select count(codcompra) into num_compras
    from compras
    where codcliente = cod;
    
    raise exception 'O cliente % fez % compras', cod, num_compras;
    raise notice 'Devolvendo o valor calculado';
    
    return num_compras;
end;
$$
language plpgsql;

select num_compras1(2);
select num_compras2(2);

-- Criar uma fun��o para que, se o total da compra for superior
-- a 10 mil, dar um desconto de 10% no valor total da compra
create function desconto(codc int) returns numeric(10,2) as
$$
declare
    total numeric(10,2);
    total_desconto numeric(10,2);
begin
    select sum(preco) into total
    from comprasitens
    where codcompra = codc;
    
    if total > 10000 then
        total_desconto := total * 0.9;
        raise notice 'O total da compra foi R$%', total;
        raise notice 'O desconto � de 10%%';
        raise notice 'O valor final � R$%', total_desconto;
    else
        total_desconto := total;
        raise notice 'O cliente n�o tem direito ao desconto.';
    end if;
    
    return total_desconto;
end;
$$ 
language plpgsql;

select desconto(1);

-- Fun��o para calcular o total de descontos dados a clientes
-- para compras que totalizaram mais que um valor
-- a partir de um valor de desconto passado como par�metro
create function total_desconto(t numeric(10, 2), 
                               des numeric(10,2)) 
returns decimal(10,2) as
$$
declare
    total_descontos numeric(10,2);
    desconto numeric(10,2);
    dados record;
begin
    total_descontos := 0.0;
    for dados in select codcompra, sum(preco) as total
                    from comprasitens
                    group by codcompra
    loop
        if dados.total > t then
            desconto := (des/100 * dados.total);
            total_descontos := total_descontos + desconto;
            raise notice 'A compra % teve desconto de %',
                dados.codcompra, desconto;
        end if;    
    end loop;
    
    return total_descontos;
end;
$$
language plpgsql;

select total_desconto(15000, 10);


-- Fun��o que recebe o c�digo de um produto e exibe a
-- quantidade de vezes que o produto foi vendido
create or replace function vezes_vendido(codp int) 
returns void as
$$
declare
    prod record;
begin
    select p.descricao, count(ci.codproduto) as nv into prod
    from produtos p, comprasitens ci
    where p.codproduto = ci.codproduto
      and p.codproduto = codp
    group by 1;
    
    raise notice 'O produto % foi vendido % vezes',
        prod.descricao, prod.nv;
end;
$$
language plpgsql;

select vezes_vendido(2);

-- Fun��o que recebe o c�digo de um cliente como par�metro
-- de entrada, e devolve o n�mero de compras e o total gasto
-- pelo cliente tamb�m por par�metros
create function compras_cliente(codc int, out num_compras int,
                         out total_gasto decimal(10, 2)) as
$$
begin
    select count(ci.codcompra), sum(ci.preco) 
        into num_compras, total_gasto
    from compras co join comprasitens ci 
        on co.codcompra = ci.codcompra 
    where co.codcliente = codc
    group by co.codcliente;
end;
$$
language plpgsql;

-- A fun��o com 2 ou mais par�metros de sa�da retorna um 
-- registro com o resultado. Para visualizar este registro
-- separado por campos, chamamos a fun��o como:
select * from compras_cliente(10);


-- Fun��o que recebe como par�metro um ano, e retorna o total 
-- de vendas por m�s (somente o total)
create function total_mes(ano int) 
    returns setof numeric(10,2) as
$$
begin
    return query select sum(ci.preco) 
                 from compras co, comprasitens ci
                 where co.codcompra = ci.codcompra
                   and extract(year from co.datacompra) = ano
                 group by extract(month from co.datacompra);
end;
$$
language plpgsql;

select total_mes(2019);


-- EM STAND BY... 
-- Modificar a fun��o anterior para que seja devolvido nome do 
-- m�s e total vendido naquela m�s
create or replace function total_mes_nome(ano int) 
    returns setof record as
$$
declare
    dados record;
begin
    for dados in select to_char(co.datacompra, 'TMMonth'),
                        sum(ci.preco) as total_mes
                 from compras co, comprasitens ci
                 where co.codcompra = ci.codcompra
                   and extract(year from co.datacompra) = ano
                 group by extract(month from co.datacompra)
                 order by extract(month from co.datacompra)
    loop
        return next dados;
    end loop;
end;
$$
language plpgsql;

select * from total_mes_nome(2019) 
    as (mes varchar(50), total_mes numeric);

-- Fun��o que recebe o c�digo de fornecedor e devolve 
-- uma listagem de seus produtos e o total vendidode cada
-- produto
create or replace function total_prod_fornecedor(codf int) 
    returns setof record as
$$
declare
    dados record;
begin
    for dados in select p.descricao,
                        sum(ci.preco) as total_prod
                 from produtos p, comprasitens ci
                 where p.codproduto = ci.codproduto
                   and p.codfornecedor = codf
                 group by p.descricao
                 order by p.descricao
    loop
        return next dados;
    end loop;
end;
$$
language plpgsql;

select * from total_prod_fornecedor(2) 
    as (descricao_produto varchar(100), total_vendido numeric);

-- Idem � anterior, por�m com retorno por par�metros
create or replace function total_prod_fornecedor2(codf int,
                out descricao_produto varchar(100), 
                out total_vendido numeric(10,2)) 
    returns setof record as
$$
declare
    dados record;
begin
    for dados in select p.descricao,
                        sum(ci.preco) as total_prod
                 from produtos p, comprasitens ci
                 where p.codproduto = ci.codproduto
                   and p.codfornecedor = codf
                 group by p.descricao
                 order by p.descricao
    loop
        descricao_produto := dados.descricao;
        total_vendido := dados.total_prod;
        return next;
    end loop;
end;
$$
language plpgsql;

select * from total_prod_fornecedor2(2);
