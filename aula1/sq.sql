--CRIANDO TABELAS
create table categoria (
	cod_categoria int primary key,
  	nome_categoria varchar(50) not null
);

create table produto(
	cod_produto int primary key,
    descricao varchar(100) not null,
    preco_custo numeric(10, 2),
    preco_venda numeric(10, 2) not null,
    data_validade date check(data_compra < data_validade),
    data_compra date,
    estoque numeric(10, 2),
    fornecedor varchar(50),
    cod_categoria int references categoria(cod_categoria)
    	on delete restrict
    	on update cascade
);

--INSERINDO VALORES
insert into categoria(cod_categoria, nome_categoria) values
	(1, 'Limpeza'),
    (2, 'Doces'),
    (3, 'Bolachas'),
    (4, 'Açougue'),
    (5, 'Quitanda');

insert into produto values
	(1, 'Sabão em pó OMO 4kg', 5.25, 8.98, '2023-02-12', '2022-02-22', 100, 'Makro', 1);
    
--CONSULTANDO VALORES
select * from categoria;
select * from produto;

--ALTERANDO TABELAS
--Modificando o nome de um campo
alter table produto rename column descricao to descricao_produto;

--Modificando o nome da tabela
alter table produto rename to produto_2;

--Acrescentando uma coluna na tabela
alter table categoria add column setor varchar(50) default 'Vendas';

--Excluindo uma coluna da tabela
alter table categoria drop column setor;

--Excluindo coluna e objetos dependentes
alter table categoria drop column cod_categoria cascade;

--Mudando tipo da coluna
alter table produto alter column fornecedor type varchar(100);

alter table categoria 
	alter column setor type numeric(10, 2) 
    using cast(setor as numeric);
    
--Excluindo valor padrão da coluna
alter table categoria alter column drop default;

--Definindo o valor padrão da coluna
alter table categoria alter column setor set default 1;

--Definindo um campo como obrigatório
alter table produto alter column estoque set not null;

--Retirando obrigatoriedade de um campo
alter table produto alter column estoque drop not null;

--Definindo campo como chave primária
alter table categoria add primary key(cod_categoria);

--Definindo campo como chave estrangeira
alter table produto
	add foreign key (cod_categoria) references categoria (cod_categoria)
    	on delete restrict on update cascade;

--Apagando uma restrição
alter table produto drop constraint produto_check;

--ATUALIZANDO REGISTROS
update categoria set cod_categoria = 1 where nome_categoria = 'Limpeza';
update categoria set cod_categoria = 2 where nome_categoria = 'Açougue';
update categoria set cod_categoria = 3 where nome_categoria = 'Quitanda';
update categoria set cod_categoria = 4 where nome_categoria = 'Doces';

--APAGANDO TABELAS
drop table produto;
drop table categoria cascade;
