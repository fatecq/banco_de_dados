create sequence seq01; 

select currval('seq01');

select nextval('seq01');

select lastval(); 

--sequ�ncia que gera valores pares entre 0 e 20, n�o c�clica
create sequence seq02 increment by 2
minvalue 0
maxvalue 20
start with 0
no cycle;

select nextval('seq02');

--sequ�ncia que gera valores de 10 a -5 com incremento -1, n�o c�clica
create sequence seq03 increment by -1
minvalue -5
maxvalue 10
start with 10
no cycle;

select nextval('seq03'); 

--sequ�ncia c�clica que gera valores entre 1 e 10
create sequence seq04
minvalue 1
maxvalue 10
cycle;

select nextval('seq04');

--usar sequ�ncia em uma tabela
create table fila (
	codigo serial primary key,
	posicao int
);

create sequence filaseq
minvalue 1
maxvalue 15
cycle;

insert into fila(posicao) values (nextval('filaseq'));

select * from fila;

--definindo o valor da sequ�ncia
select currval('seq01');

select setval('seq01', 10); --define o valor atual da sequ�ncia
select nextval('seq01');

select setval('seq01', 10, false); --define o pr�ximo valor da sequ�ncia
select nextval('seq01');



