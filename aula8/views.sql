--Criar uma vis�o para listar o nome do cliente, o c�digo e data
--das suas compras, e o valor total de cada compra
create view compras_clientes(nome_cliente, codigo_compra, data_compra, total_compra) as
	select c.nome, co.codcompra, co.datacompra, sum(ci.preco)
	from clientes c
	join compras co on (c.codcliente = co.codcliente)
	join comprasitens ci on (co.codcompra = ci.codcompra)
	group by 1, 2, 3
	order by 1, 3;
	
select * from compras_clientes;

--Vis�o materializada que lista o nome do cliente, a quantidade de compras
--e o total gasto em todas as compras
create materialized view clientes_total_gasto(codigo_cliente, nome_cliente, qtde_compras, total_gasto) as
	select c.codcliente, c.nome, count(co.codcompra), sum(ci.preco)
	from clientes c
	join compras co on (c.codcliente = co.codcliente)
	join comprasitens ci on (co.codcompra = ci.codcompra)
	group by 1, 2
	order by 2;

select * from clientes_total_gasto;

--Atualizando uma vis�o materializada
refresh materialized view clientes_total_gasto;

--Obs: Views materializadas s� atualizam os valores quando elas mesmas forem atualizadas
	

