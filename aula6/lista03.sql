--1) Exibir a quantidade de produtos vendidos por cada fornecedor
select f.nome as fornecedor, count(p.*) as total_produtos
from fornecedores f
join produtos p on (f.codfornecedor = p.codfornecedor)
group by f.nome
order by f.nome;

--2) Exibir a quantidade de compras feita por cada cliente
select c.nome as cliente, count(co.*) as total_compras
from clientes c 
join compras co on (c.codcliente = co.codcliente)
group by c.nome
order by c.nome;

--3) Exibir o nome do cliente, a data da compra, a quantidade de itens comprados e o total 
--da compra (somat�rio do campo preco da tabela ComprasItens). Ordene pelo nome 
--do cliente de forma crescente, e por data de forma decrescente
select 
	c.nome, co.datacompra, 
	count(ci.*) as qtd_itens_comprados,
	sum(ci.preco) as valor_total_compra
from clientes c
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
group by c.nome, co.datacompra
order by c.nome, co.datacompra desc;

--4) Exibir o Nome do fornecedor, a descri��o do produto e a quantidade total vendida de 
--cada produto. Ordene pelo nome do fornecedor e descri��o do produto
select 
	f.nome as fornecedor, p.descricao as produto,
	sum(ci.quantidade) as total_vendido
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
join comprasitens ci on (p.codproduto = ci.codproduto)
group by f.nome, p.descricao
order by f.nome, p.descricao;

--5) Exibir o total de vendas por estado
select c.estado, count(co.*) as total_vendas
from clientes c
join compras co on (c.codcliente = co.codcliente)
group by c.estado;

--6) Exibir o total de produtos vendidos por estado
select c.estado, sum(ci.quantidade) as total_produtos_vendidos
from clientes c
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
group by c.estado;

--7) Exibir a descri��o do produto e a quantidade de clientes que compraram este produto
select p.descricao as produto, count(distinct c.codcliente) as total_clientes
from produtos p 
join comprasitens ci on (ci.codproduto = p.codproduto)
join compras co on (ci.codcompra = co.codcompra)
join clientes c on (co.codcliente = c.codcliente)
group by p.descricao;

--8) Exibir o nome dos fornecedores que venderam no total, mais de 10 produtos
select f.nome as fornecedor, count(p.*) as total_produtos
from fornecedores f
join produtos p on (f.codfornecedor = p.codfornecedor)
group by f.nome having count(p.*) > 10
order by f.nome;

--9) Exibir o nome do fornecedor, o pre�o de venda do produto mais caro e do produto 
--mais barato e o pre�o m�dio dos produtos vendidos por este fornecedor
select 
	f.nome as fornecedor, 
	max(p.precovenda) as mais_caro,
	min(p.precovenda) as mais_barato,
	round(avg(p.precovenda), 2) as preco_medio
from fornecedores f
join produtos p on (f.codfornecedor = p.codfornecedor)
group by f.nome
order by f.nome;

--10) Exibir o nome dos clientes que gastaram mais que 100 reais no m�s de junho de 
--2010
select c.nome
from clientes c 
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
where co.datacompra between '2010-06-01' and '2010-06-30'
group by c.nome having sum(ci.preco) > 100;

--11) Exibir o nome do cliente, e o valor m�dio de suas compras.
select c.nome, round(avg(ci.preco), 2) as valor_medio_compras
from clientes c
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
group by c.nome;

--12) Exibir no nome do cliente, o nome do fornecedor, a descri��o do produto e a 
--quantidade total de itens do produto comprado pelo cliente
select 
	c.nome as cliente, f.nome as fornecedor, 
	p.descricao as produto, sum(ci.quantidade)
from clientes c 
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
join produtos p on (ci.codproduto = p.codproduto)
join fornecedores f on (p.codfornecedor = f.codfornecedor)
group by c.nome, f.nome, p.descricao
order by c.nome;

--13) Exibir os tr�s clientes que mais gastaram na loja
select c.nome
from clientes c
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
group by c.nome
order by sum(ci.preco) desc
limit 3;

--14) Exibir os cinco produtos menos vendidos na loja
select p.descricao as produto
from produtos p
join comprasitens ci on (p.codproduto = ci.codproduto)
group by p.descricao
order by sum(ci.quantidade) asc limit 5;

--15) Utilizando LIMIT e OFFSET e sem utilizar nenhuma fun��o de agrega��o, crie 
--consultas para exibir o produto mais caro e o produto mais barato (2 consultas)
select p.descricao as produto_mais_caro, p.precovenda 
from produtos p 
order by p.precovenda desc
limit 1;

select p.descricao as produto_mais_barato, p.precovenda 
from produtos p 
order by p.precovenda asc
limit 1;


