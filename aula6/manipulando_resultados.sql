--ORDER BY - Ordenando resultados
select 
	f.nome, p.descricao, p.precocusto, p.precovenda, 
	round((100 * ((p.precovenda - p.precocusto) / p.precocusto)), 3) as lucro
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
order by f.nome, p.descricao;
----OU-----
select 
	f.nome, p.descricao, p.precocusto, p.precovenda, 
	round((100 * ((p.precovenda - p.precocusto) / p.precocusto)), 3) as lucro
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
order by 1, 2;

--Para a mesma consulta, ordenar pelo nome do fornecedor, e de forma
--descrescente pelo lucro
select 
	f.nome, p.descricao, p.precocusto, p.precovenda, 
	round((100 * ((p.precovenda - p.precocusto) / p.precocusto)), 3) as lucro
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
order by 1, 5 desc;


--LIMIT/OFFSET - Exibindo n�mero limitado de resultados
--Para a mesma consulta, exibir os 20 primeiros resultados
select 
	f.nome, p.descricao, p.precocusto, p.precovenda, 
	round((100 * ((p.precovenda - p.precocusto) / p.precocusto)), 3) as lucro
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
order by 1, 2
limit 20; 

--Para a mesma consulta, pular os 20 primeiros resultados
select 
	f.nome, p.descricao, p.precocusto, p.precovenda, 
	round((100 * ((p.precovenda - p.precocusto) / p.precocusto)), 3) as lucro
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
order by 1, 2
offset 20; 

--Para a mesma consulta, exibir os resultados de 21 a 45
select 
	f.nome, p.descricao, p.precocusto, p.precovenda, 
	round((100 * ((p.precovenda - p.precocusto) / p.precocusto)), 3) as lucro
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
order by 1, 2
limit 25 offset 20; 


--GROUP BY - Agrupando resultados
--Exibir a cidade e o estados dos clientes
select estado
from clientes
group by estado;

--FUN��ES DE AGREGA��O
--COUNT
--Exibir a quantidade de clientes
select count(*)
from clientes;

--Exibir a quantidade de estados cadastrados dos clientes
select count(estado)
from clientes;

--Exibir a quantidade de estados distintos dos clientes
select count(distinct estado)
from clientes;

--SUM - Somando valores
--Qual o valor total da compra de c�digo 1?
select sum(preco)
from comprasitens
where codcompra = 1;

--AVG - Calculando a m�dia
--Qual o pre�o de venda m�dia dos produtos do fornecedor de c�digo 5?
select avg(precovenda)
from produtos
where codfornecedor = 5;
----OU----
select round(avg(precovenda), 2)
from produtos
where codfornecedor = 5;

--MIN - Obtendo o menor valor
--Qual a data da compra mais antiga?
select min(datacompra)
from compras;

--MAX - Obtendo o maior valor
--Qual a data da compra mais recente?
select max(datacompra)
from compras;


--FUN��ES DE AGREGA��O E GROUP BY
--Exibir a quantidade de clientes por cidade, e ordenar o resultado de forma
--decrescente pela quantidade
select cidade, count(cidade) as num_clientes
from clientes c 
group by cidade 
order by 2 desc;

--Exibir o nome do cliente, os c�digos de suas compras, e valor total
--da compra, ordenando o resultado pelo nome do cliente, e pelo valor
--da compra de forma decrescente
select c.nome, co.datacompra, sum(ci.preco)
from clientes c 
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
group by c.nome, co.datacompra 
order by c.nome, 3 desc;

--Exibir o nome do fornecedor, e o valor m�dio do pre�o de venda
--de seus produtos
select f.nome, avg(p.precovenda)
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
group by 1; 

--Exibir nome do cliente, o c�digo e a data das compras que totalizaram
--mais que 25000
select c.nome, co.codcompra, co.datacompra, sum(ci.preco) as total_compra
from clientes c 
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
group by 1, 2, 3
having sum(ci.preco) >= 25000;

--Calcular o total vendido de todos os produtos que custam mais que 100 reais
select p.descricao, sum(ci.preco)
from comprasitens ci
join produtos p on (p.codproduto = ci.codproduto)
where ci.preco >= 100
and sum(ci.preco) < 5000
group by p.descricao;
-- este comando n�o vai funcionar, pois o resultado da agrega��o � feito apenas ap�s
-- o agrupamento (group by)

