select p.descricao, p.precovenda
from produtos p
where p.precovenda between 3 and 5;

--LIKE e ILIKE (LIKE é case sensitive)
--listar produtos que contenham a palavra 'sal'
select p.descricao
from produtos p
where p.descricao ilike '%sal%';

--listar produtos que iniciem com 'sal'
select p.descricao
from produtos p
where p.descricao ilike 'sal%';

--listar produtos que a descrição se inicie com 'C' e possuam 'r' como terceiro caracter
select p.descricao
from produtos p
where p.descricao ilike 'c_r%';

--listar produtos que, com um aumento de 10% no preço de venda, custariam entre 20 e 30 reais
select 
	p.descricao, p.precovenda as preco_original,
    (p.precovenda * 1.1) as preco_reajustado
from produtos p
where (p.precovenda * 1.1) between 20 and 30;

--JUNÇÕES WHERE
--listar o nome do cliente e o código de suas compras
select c.nome, co.codcompra
from clientes c, compras co
where c.codcliente = co.codcliente;

--listar o nome do cliente e o código de suas compras, para os clientes de São Paulo
select 
	c.nome, co.codcompra
from 
	clientes c, compras co
where 
	c.codcliente = co.codcliente
    and c.estado = 'SP';
    
--listar o nome do fornecedor, a descrição do produto e a quantidade comprada
select 
	f.nome, p.descricao, ci.quantidade
from 
	fornecedores f, produtos p, comprasitens ci
where 
	f.codfornecedor = p.codfornecedor 
    and p.codproduto = ci.codproduto
order by 
	f.nome, p.descricao;

select 
	f.nome, p.descricao, sum(ci.quantidade) as quantidade
from 
	fornecedores f, produtos p, comprasitens ci
where 
	f.codfornecedor = p.codfornecedor 
    and p.codproduto = ci.codproduto
group by 
	f.codfornecedor, p.codproduto
order by 
	f.nome, p.descricao;

--listar a descrição do produto e a data em que o produto foi comprado
select p.descricao, co.datacompra
from produtos p, compras co, comprasitens ci 
where 
	p.codproduto = ci.codproduto
    and co.codcompra = ci.codcompra;

--JUNÇÕES JOIN
--listar o nome do cliente e o código de suas compras
select c.nome, co.codcompra
from clientes c
join compras co on (c.codcliente = co.codcliente);

/*
  * Obs: quando a chave primária e a chave estrangeira da junção tiverem o mesmo nome, pode-se substituir o 'on' e a condição 
  * por 'using' e o nome do campo. Exemplo:
  
  *  select c.nome, co.codcompra
  *  from clientes c
  *  join compras co using (codcliente);
*/

--listar o nome do fornecedor, a descrição do produto e a quantidade comprada
select f.nome, p.descricao, ci.quantidade
from fornecedores f
join produtos p on (f.codfornecedor = p.codfornecedor)
join comprasitens ci on (p.codproduto = ci.codproduto);

--listar clientes que efetuaram compras após o dia 01/04/2019
select 
	c.nome, co.datacompra
from 
	clientes c
    join compras co on (c.codcliente = co.codcliente)
where 
	co.datacompra > '2019-04-01';
    
--listar o nome do fornecedor, a descrição e o preço de venda do produto, apenas para os fornecedores do estado de MG
select 
	f.nome, p.descricao, p.precovenda
from 
	fornecedores f
    join produtos p on (f.codfornecedor = p.codfornecedor)
where 
	f.estado = 'MG';