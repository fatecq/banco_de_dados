create table ambulatorios(
	nroa int,
    andar numeric(3) not null,
    capacidade smallint
);
--PK
alter table ambulatorios add primary key(nroa);

create table medicos(
	codm int,
    nome varchar(40) not null,
	idade smallint not null,
	especialidade char(20),
	CPF char(11) unique,
	cidade varchar(50),
	nroa int
);
--PK
alter table medicos add primary key(codm);
--FK
alter table medicos add foreign key(nroa) references ambulatorios(nroa)
    on delete cascade on update cascade;

create table pacientes(
	codp int,
	nome varchar(40) not null,
	idade smallint not null,
	cidade char(30),
	CPF char(11) unique,
	doenca varchar(40) not null
);
--PK
alter table pacientes add primary key(codp);

create table funcionarios(
	codf int,
	nome varchar(40) not null,
	idade smallint,
	CPF char(11) unique,
	cidade varchar(30),
	salário numeric(10),
	cargo varchar(20)
);
--PK
alter table funcionarios add primary key(codf);

create table consultas(
	codc serial,
	codm int,
	codp int,
	data date,
	hora time
);
--PK
alter table consultas add primary key(codc);
--FK
alter table consultas add foreign key(codm) references medicos(codm)
    on delete cascade on update cascade;
alter table consultas add foreign key(codp) references pacientes(codp)
    on delete cascade on update cascade;

--Adicionando campo na tabela funcionários
alter table funcionarios add column nroa int;
--Adicionando FK na tabela funcionários
alter table funcionarios add foreign key(nroa) references ambulatorios(nroa)
    on delete cascade on update cascade;

--Removendo campos da tabela funcionários
alter table funcionarios drop column cargo;
alter table funcionarios drop column nroa;

--Populando as tabelas
insert into ambulatorios values 
	(1, 1, 30),
    (2, 1, 50),
    (3, 2, 40),
    (4, 2, 25),
    (5, 2, 55);

insert into medicos values
	(1, 'Joao', 40, 'ortopedia', '10000100000', 'Florianopolis', 1),
    (2, 'Maria', 42, 'traumatologia', '10000110000', 'Blumenal', 2),
    (3, 'Pedro', 51, 'pediatria', '11000100000', 'São José', 2),
    (4, 'Carlos', 28, 'ortopedia', '11000110000', 'Joinville', null),
    (5, 'Marcia', 33, 'neurologia', '11000111000', 'Biguacu', 3);
    
insert into pacientes values
	(1, 'Ana', 20, 'Florianopolis', '20000200000', 'gripe'),
    (2, 'Paulo', 24, 'Palhoça', '20000220000', 'fratura'),
    (3, 'Lucia', 30, 'Biguaçu', '22000200000', 'tendinite'),
    (4, 'Carlos', 28, 'Joinville', '11000110000', 'sarampo');
    
insert into funcionarios values
	(1, 'Rita', 32, '20000100000', 'Sao Jose', 1200),
    (2, 'Maria', 55, '30000110000', 'Palhoca', 1220),
    (3, 'Caio', 45, '41000100000', 'Florianopolis', 1100),
    (4, 'Carlos', 44, '51000110000', 'Florianopolis', 1200),
    (5, 'Paula', 33, '61000111000', 'Florianopolis', 2500);
    
insert into consultas(codm, codp, data, hora) values
	(1, 1, '2006-06-12', '14:00'),
    (1, 4, '2006-06-13', '10:00'),
    (2, 1, '2006-06-13', '9:00'),
    (2, 2, '2006-06-13', '11:00'),
    (2, 3, '2006-06-14', '14:00'),
    (2, 4, '2006-06-14', '17:00'),
    (3, 1, '2006-06-19', '18:00'),
    (3, 3, '2006-06-12', '10:00'),
    (3, 4, '2006-06-19', '13:00'),
    (4, 4, '2006-06-20', '13:00'),
    (4, 4, '2006-06-22', '19:30');

--O paciente Paulo mudou-se para Ilhota
update pacientes set cidade = 'Ilhota' where codp=2;

--A consulta do médico 1 com o paciente 4 passou para às 12:00 horas do dia 4 de Julho de 2006
update consultas set data = '2006-07-04', hora = '12:30' where codm = 1 and codp=4;

--A doença da paciente Ana agora é fratura
update pacientes set doenca = 'fratura' where codp=1;

--A consulta do médico Pedro (codf = 3) com o paciente Carlos (codf = 4) passou para uma hora e meia depois
update consultas set hora = hora + '01:30' where codm = 3 and codp = 4;

--O funcionário Carlos (codf = 4) deixou a clínica
delete from funcionarios where codf = 4;

--As consultas marcadas após as 19 horas foram canceladas
delete from consultas where hora > '19:00';

--Os pacientes com sarampo ou idade inferior a 10 anos deixaram a clínica
delete from pacientes where doenca = 'sarampo' or idade < 10;

--Os médicos que residem em Biguacu e Palhoca deixaram a clínica
delete from medicos where cidade = 'Biguacu' or cidade = 'Palhoca';


    

