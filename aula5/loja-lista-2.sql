--1) Exibir todos os clientes cadastrados do estado de SP
select * from clientes where estado = 'SP';

--2) Exibir todos os fornecedores de fora do estado de SP
select * from fornecedores where estado <> 'SP';

--3) Exibir a descri��o e o pre�o de venda dos produtos cadastrados
select descricao, precoVenda from produtos;

--4) Exibir a data das compras de um cliente (escolha um c�digo de cliente cadastrado)
select dataCompra from compras where codcliente = 1;

--5) Exibir todos os produtos vendidos por um fornecedor (escolha um c�digo de 
-- fornecedor cadastrado)
select * from produtos where codfornecedor = 1;

--6) Exibir a quantidade e o pre�o dos itens de uma compra (escolha um c�digo de compra 
-- cadastrado)
select quantidade, preco from comprasitens where codcompra = 1;

--7) Listar todos os produtos que custem entre 10 e 50 reais
select * from produtos where precovenda between 10 and 50;

--8) Listar todos os clientes cujo nome comece com �Ana� (ou outro cadastrado)
select * from clientes where nome ilike 'Ana%';

--9) Listar todos os clientes cuja cidade termine em �ta� (ou outra cadastrada)
select * from clientes where cidade like '%ta';

--10) Listar todos os produtos que possuam a palavra �lata� em sua descri��o 
-- (ou outra escolhida)
select * from produtos where descricao like '%lata%';

--11) Adicione ao banco dois clientes do estado do Esp�rito Santo (ES). N�o cadastre compras 
-- para estes clientes
insert into clientes values
	(31, 'Lav�nia Stella La�s Moura', 'Rua Beruri', 'Brejo da Guabiraba', '52291-203', 'Recife', 'PE', '2022-04-12'),
    (32, 'Marcelo Elias Geraldo Ara�jo', 'Avenida Vereador Laudelino Schettino', 'Democrata', '36035-205', 'Juiz de Fora', 'MG', '2022-04-12');

--12) Exibir o nome, a cidade, o c�digo e a data das compras (escolha um cliente cadastrado)
select c.nome, c.cidade, co.codcompra, co.datacompra
from clientes c
join compras co on (c.codcliente = co.codcliente)
where c.codcliente = 2;

--13) Exibir os dados os clientes que efetuaram compras ap�s o dia 01/04/2019
select c.* from clientes c 
join compras co on (c.codcliente = co.codcliente)
where co.datacompra > '2019-04-01';

--14) Exibir o nome de todos fornecedores, seu estado e a descri��o de seus produtos
select f.nome, f.estado, p.descricao
from fornecedores f
join produtos p on (f.codfornecedor = p.codfornecedor)
order by f.nome, p.descricao;

--15) Idem ao anterior, mas para apenas um fornecedor cadastrado
select f.nome, f.estado, p.descricao
from fornecedores f
join produtos p on (f.codfornecedor = p.codfornecedor)
where f.codfornecedor = 2
order by p.descricao;

--16) Exibir o c�digo do cliente, seu nome, o c�digo e a data da compra e, a quantidade e o 
-- pre�o dos itens que comp�em cada compra
select 
	c.codcliente, c.nome,
    co.codcompra, co.datacompra,
    ci.quantidade, ci.preco
from 
	clientes c
    join compras co on (c.codcliente = co.codcliente)
    join comprasitens ci on (co.codcompra = ci.codcompra)
order by 
	c.codcliente, co.codcompra;

--17) Altere a consulta anterior para exibir as informa��es de um cliente e apenas uma de 
-- suas compras 
select 
	c.codcliente, c.nome,
    co.codcompra, co.datacompra,
    ci.quantidade, ci.preco
from 
	clientes c
    join compras co on (c.codcliente = co.codcliente)
    join comprasitens ci on (co.codcompra = ci.codcompra)
where
	c.codcliente = 1 and co.codcompra = 3;
    
--18) Exibir o nome do fornecedor, a descri��o e o pre�o de venda do produto dos fornecedores 
-- do estado de MG
select f.nome, p.descricao, p.precovenda
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor)
where f.estado = 'MG';

--19) Exibir o c�digo da compra, sua data, a descri��o do produto, a quantidade comprada 
-- e o pre�o (escolha uma compra cadastrada)
select c.codcompra, c.datacompra, p.descricao, ci.quantidade, ci.preco
from compras c
join comprasitens ci on (c.codcompra = ci.codcompra)
join produtos p on (p.codproduto = ci.codproduto)
where c.codcompra = 1;

--20) Altere a consulta anterior, incluindo o nome do cliente
select co.codcompra, c.nome, co.datacompra, p.descricao, ci.quantidade, ci.preco
from clientes c
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
join produtos p on (p.codproduto = ci.codproduto)
where co.codcompra = 1;

--21) Altere a consulta anterior, incluindo o nome do fornecedor do produto
select 
	co.codcompra, c.nome as cliente, co.datacompra,
	p.descricao, f.nome as fornecedor, 
	ci.quantidade, ci.preco
from clientes c
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
join produtos p on (p.codproduto = ci.codproduto)
join fornecedores f on (f.codfornecedor = p.codfornecedor)
where co.codcompra = 1;

--22) Exibir os campos nome do fornecedor, descri��o do produto, pre�o de custo, pre�o de 
-- venda e um campo adicional que informa o lucro obtido na venda 
-- ( 100 * ((Pre�o de Venda � Pre�o de Custo) / Pre�o de Custo )
select 
	f.nome, p.descricao, p.precocusto, p.precovenda, 
	round((100 * ((p.precovenda - p.precocusto) / p.precocusto)), 3) as lucro
from fornecedores f 
join produtos p on (f.codfornecedor = p.codfornecedor);


--23) Exibir o c�digo e o nome dos clientes de SP que compraram produtos de 
-- fornecedores de MG
select distinct c.codcliente, c.nome
from clientes c 
join compras co on (c.codcliente = co.codcliente)
join comprasitens ci on (co.codcompra = ci.codcompra)
join produtos p on (ci.codproduto = p.codproduto)
join fornecedores f on (p.codfornecedor = f.codfornecedor)
where c.estado = 'SP' and f.estado = 'MG';

--24) Exibir a descri��o dos produtos que n�o foram vendidos
select p.descricao
from produtos p 
left join comprasitens ci on (p.codproduto = ci.codproduto)
where ci.codcompraitem is null;

--25) Exibir os clientes que n�o fizeram nenhuma compra
select c.*
from clientes c
left join compras co on (c.codcliente = co.codcliente)
where co.codcompra is null;
    