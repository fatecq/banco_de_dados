--Exibir todos os clientes que não fizeram compras
select c.nome, co.codcompra
from clientes c left join compras co on c.codcliente = co.codcliente
where co.codcompra is null;

--Não retorna nada, pois todas as compras estão vinculadas a um cliente
select c.nome, co.codcompra
from clientes c right join compras co on c.codcliente = co.codcliente
where co.codcompra is null;

--Listar clientes e fornecedores que não estão no mesmo estado
select c.nome, f.nome
from clientes c join fornecedores f on c.estado <> f.estado
order by 1;

--Exibir o nome da biblioteca e dos usuários que estão na mesma cidade da biblioteca
select b.nome, u.nome
from (bibliotecas b join enderecos eb on b.codendereco = eb.codendereco)
join (usuarios u join enderecos eu on u.codendereco = eu.codendereco)
on eb.cidade = eu.cidade;

