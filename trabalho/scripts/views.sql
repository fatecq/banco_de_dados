/*
 * Trabalho final - Laboratorio de Banco de Dados
 * Alunos: Gabriel N. Quilice e Wesley Felipe Vieira 
 */
create view produtos_vendidos_mes as
	select 
		p.nome as produto,
		fo.nome as fornecedor,
		sum(vi.quantidade) as total_vendido
	from 
		produto p
		join fornecedor fo on (p.f_idfornecedor = fo.idfornecedor)
		join venda_item vi on (vi.p_idproduto = p.idproduto)
		join venda v on (vi.v_idvenda = v.idvenda)
	where
		extract (month from v.data_venda) = extract(month from now())
	group by 1, 2
	order by p.nome;

create view produtividade_funcionario as 
	select 
		f.nome as funcionario, 
		count(distinct v.idvenda) as total_vendas,
		sum(vi.quantidade) as total_itens_vendidos
	from
		funcionario f
		join venda v on (v.f_idfuncionario = f.idfuncionario)
		join venda_item vi on (v.idvenda = vi.v_idvenda)
	group by 1
	order by f.nome;

 create view relatorio_venda as 
	select 
		v.idvenda, f.nome as funcionario,
		v.preco_venda as valor_total_venda,
		tp.descricao as tipo_pagamento,
		p.nome as produto, fo.nome as fornecedor,
		vi.quantidade as qtd_itens,
		vi.total_preco_item as sub_total
	from 
		venda v
		join funcionario f on (v.f_idfuncionario = f.idfuncionario)
		join tipo_pagamento tp on (v.tp_idpagamento = tp.idtipo)
		join venda_item vi on (vi.v_idvenda = v.idvenda)
		join produto p on (vi.p_idproduto = p.idproduto)
		join fornecedor fo on (p.f_idfornecedor = fo.idfornecedor)
	order by v.idvenda, p.nome;