/*
 * Trabalho final - Laboratorio de Banco de Dados
 * Alunos: Gabriel N. Quilice e Wesley Felipe Vieira 
 */
create table funcionario(
	idfuncionario serial primary key,
	nome varchar(45) not null,
	cpf char(14) not null,
	rg char(16) not null,
	sexo char(1) check (sexo in ('M', 'F')) not null,
	data_nascimento date not null,
	usuario varchar(45) unique not null,
	senha varchar(45) not null,
	fg_ativo char(1) check (fg_ativo in ('S', 'N')) not null
);

create table fornecedor(
	idfornecedor serial primary key,
	nome varchar(45) not null,
	estado char(2) not null,
	telefone varchar(11)
);

create table produto(
	idproduto serial primary key,
	f_idfornecedor int not null,
	nome varchar(200) not null,
	codigo_barras varchar(45),
	qt_estoque int not null,
	preco decimal(10, 2) not null,
	fg_ativo char(1) check (fg_ativo in ('S', 'N')) not null,
	foreign key (f_idfornecedor) references fornecedor(idfornecedor) on delete restrict on update cascade
);

create table tipo_pagamento(
	idtipo serial primary key,
	descricao varchar(45) not null
);

create sequence vendaseq increment by 1 start with 1;

create table venda(
	idvenda int primary key default nextval('vendaseq'),
	f_idfuncionario int not null,
	tp_idpagamento int not null,
	data_venda date not null,
	preco_venda decimal(10, 2) not null,
	foreign key (f_idfuncionario) references funcionario(idfuncionario) on delete restrict on update cascade,
	foreign key (tp_idpagamento) references tipo_pagamento(idtipo) on delete restrict on update cascade
);

create table venda_item(
	v_idvenda int not null,
	p_idproduto int not null,
	quantidade int not null,
	total_preco_item decimal(10, 2) not null,
	foreign key (v_idvenda) references venda(idvenda) on delete restrict on update cascade,
	foreign key (p_idproduto) references produto(idproduto) on delete restrict on update cascade
);

create table estatisticas (
	cod_tabela serial primary key,
	nome_tabela varchar(100) not null,
	num_registros int not null
);