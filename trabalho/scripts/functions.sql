/*
 * Trabalho final - Laboratorio de Banco de Dados
 * Alunos: Gabriel N. Quilice e Wesley Felipe Vieira 
 */
create or replace function total_vendas(id_funcionario int) returns int as
$$
declare
    num_vendas int;
begin
    select count(v.idvenda) into num_vendas
    from venda v
    where f_idfuncionario = id_funcionario;
    
    return num_vendas;
end;
$$
language plpgsql;

create or replace function lucratividade_mes(ano_base int) 
    returns setof text as
$$
begin
    return query select extract(month from v.data_venda) || ' - ' || sum(v.preco_venda) 
    			 from venda v 
    			 where extract(year from v.data_venda) = ano_base
                 group by extract(month from v.data_venda);
end;
$$
language plpgsql;

create or replace function total_vendido_produto() 
    returns setof record as
$$
declare
    prod record;
begin
    for prod in select p.nome, p.qt_estoque, p.preco, sum(vi.quantidade) as total_vendido 
				 from produto p
				  join venda_item vi on (p.idproduto = vi.p_idproduto)
				 group by p.idproduto
				 order by 1, 4
    loop
        return next prod;
    end loop;
end;
$$
language plpgsql;

create or replace function produtos_estoque_menor(qtd int, out produto varchar(200), out fornecedor varchar(45), out qtd_estoque int) 
    returns setof record as
$$
declare
    x record;
begin
    for x in (select p.*, fo.nome as fornecedor 
		    	from produto p 
		    		join fornecedor fo on (p.f_idfornecedor = fo.idfornecedor)
		    	where qt_estoque <= qtd)
    loop
	    produto := x.nome;
	    fornecedor := x.fornecedor;
	    qtd_estoque := x.qt_estoque;
        return next;
    end loop;
end;
$$
language plpgsql;

create or replace function set_estatisticas(valor varchar(100), out tabela varchar(100), out num_registros varchar(45)) 
    returns setof record as
$$
declare
    x record;
    existe_tabela int;
    total_registros int;
begin
	if valor = '*' then
		for x in (select table_name from information_schema.tables where table_type = 'BASE TABLE' and table_schema = 'public')
	    loop
		    execute 'select count(*) from ' || x.table_name into total_registros;
		    select count(*) into existe_tabela from estatisticas where nome_tabela = x.table_name;
		   
		    if existe_tabela > 0 then
		      update estatisticas set num_registros = total_registros where nome_tabela = x.table_name;
		    else 
		      insert into estatisticas(nome_tabela, num_registros) values (x.table_name, total_registros);
		    end if;
		   
		    tabela := x.table_name;
		    num_registros:= total_registros;
	        return next;
	    end loop;
	else
		select count(*) into existe_tabela from information_schema.tables where table_type = 'BASE TABLE' and table_schema = 'public' and table_name = lower(valor);
		if existe_tabela = 0 then 
			raise exception 'Este não é o nome de uma tabela que exista no banco de dados!';
		end if;
	
		execute 'select count(*) from ' || lower(valor) into total_registros;
		select count(*) into existe_tabela from estatisticas where nome_tabela = lower(valor);
		   
		if existe_tabela > 0 then
		  update estatisticas set num_registros = total_registros where nome_tabela = lower(valor);
		else 
		  insert into estatisticas(nome_tabela, num_registros) values (lower(valor), total_registros);
		end if;
		   
		tabela := lower(valor);
		num_registros:= total_registros;
	    return next;
	end if;
end;
$$
language plpgsql;